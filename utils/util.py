import time
from os import makedirs
from os.path import dirname
from os.path import exists as p_exists, join as p_join, isfile as p_isfile, isdir as p_isdir, getctime
import logging

logger = logging.getLogger(__name__)

def is_file(path):
    return p_exists(path) and p_isfile(path)


def is_directory(path):
    return p_exists(path) and p_isdir(path)


def make_directories_for_file(path):
    directory = dirname(path)

    if not p_exists(directory):
        makedirs(directory)


class TimeMeasure(object):
    def __init__(self, enter_msg="", exit_msg="{}.", writer=logger.debug, print_enabled=True):
        self.__enter_msg = enter_msg
        self.__exit_msg = exit_msg
        self.__writer = writer
        self.__time = None
        self.__print_enabled = print_enabled

    def __enter__(self):
        self.__start = time.time()
        if self.__print_enabled and self.__enter_msg:
            self.__writer(self.__enter_msg)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.__print_enabled:
            self.__writer(self.__exit_msg.format(pretty_time_interval(self.delta)))

    @property
    def delta(self):
        delta = time.time() - self.__start
        delta = int(delta * 1000)
        return delta

def pretty_time_interval(millis):
    seconds, millis = divmod(millis, 1000)
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)
    return f"{days}d {hours}h {minutes}min {seconds}sec {millis}ms"
