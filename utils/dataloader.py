from torch.utils.data import Dataset, DataLoader, random_split
from torch.utils.data.dataset import Subset
from sklearn.model_selection import KFold
from .util import TimeMeasure, is_file, make_directories_for_file
import logging
import os
import torch
from PIL import Image

logger = logging.getLogger(__name__)


class ImageMetaData(object):

    def __init__(self, label, path, image):
        self.__label = label
        self.__path = path
        self.__image = image

    def get_path(self):
        return self.__path

    def get_class(self):
        return self.__label

    def get_image(self):
        return self.__image

class ImageDataSet(Dataset):

    def __init__(self, root_dir, include_unk=True, transform=None, load_on_request=True, return_path=False, unk_limit=None, testing=False):
        """
        root_dir: directory of the dataset
        include_unk: Whether to include the unknown class
        transform: transormations to be applied every time a batch is loaded
        """
        self.__root_dir = root_dir
        self.__transform = transform
        self.__meta = list()
        if not unk_limit is None:
            self.unk_limit = unk_limit
        else:
            self.unk_limit = 1e15
        self.__testing = testing
        self.class_numbers = {"AK": 0,
                               "BCC": 1,
                               "BKL":2,
                               "DF":3,
                               "MEL":4,
                               "NV":5,
                               "SCC":6,
                               "VASC":7}
        if include_unk:
            self.class_numbers["UNK"] = 8
        self.load_on_request = load_on_request
        self.return_path = return_path

        with TimeMeasure(enter_msg="Begin meta data loading.",
                         exit_msg="Finished meta data loading after {}.",
                         writer=logger.debug):
            self.__process_meta()



    def __process_meta(self):
        if self.__testing:
            for filename in os.listdir(self.__root_dir):
                image = None
                path = os.path.join(self.__root_dir, filename)
                if not self.load_on_request:
                    image = torch.from_numpy(cv2.imread(path))
                self.__meta.append(ImageMetaData(0, path, image))
            return
                
        for classname in os.listdir(self.__root_dir):
            label = self.class_numbers.get(classname)
            if label is None:
                logger.error("Warning: Undefined class name {} in data directory {}".format(classname, self.__root_dir))
            for num_img, filename in enumerate(os.listdir(os.path.join(self.__root_dir, classname))):
                image = None
                path = os.path.join(self.__root_dir, classname, filename)
                if not self.load_on_request:
                    image = torch.from_numpy(cv2.imread(path))
                if label == 8 and num_img > self.unk_limit:
                    break
                self.__meta.append(ImageMetaData(label, path, image))



    def __len__(self):
        return len(self.__meta)

    def __getitem__(self, idx):
        if type(idx) == torch.Tensor:
            idx = idx.item()
        meta = self.__meta[idx]
        path = None
        if self.load_on_request:
            path = meta.get_path()
            image = Image.open(path)
        else:
            image = meta.get_image()

        if self.__transform is not None:
            image = self.__transform(image)

        if self.return_path:
            return image, meta.get_class(), path
        return image, meta.get_class()


    def get_balanced_weights(self):
        nclasses = len(self.class_numbers.keys())
        count = [0] * nclasses
        for meta in self.__meta:
            count[meta.get_class()] += 1
        weight_per_class = [0.] * nclasses
        N = float(sum(count))
        for i in range(nclasses):
            weight_per_class[i] = N/float(count[i])
        weight = [0] * len(self.__meta)
        for idx, meta in enumerate(self.__meta):
            weight[idx] = weight_per_class[meta.get_class()]
        return weight, weight_per_class
