# [ScanYourSkin](https://scanyourskin.vision/)

## Dataset

- [combined dataset](https://drive.google.com/uc?id=1xcXD0duiJMfA5gw6NION3AuSUAB3XWcZ) (~13 GB)
- [cropped, split & augmented dataset](https://drive.google.com/uc?id=1_Er56yNs3dB30rYjwx-6eBsGkrMFnwuk) (~2.3 GB)

### preprocessing
 1. combine all datasets (run `jupyter notebook dataset/grouping_datasets.ipynb`)
 2. all images are center cropped to 512x512 pixels (`python dataset/crop_images.py`)
 3. split images into train(60%), test(20%) and valid(20%) according to [Deep Learning with Python: A Hands-on Introduction, 2017](https://books.google.de/books?id=19CyDgAAQBAJ&pg=PA219) (`python dataset/split.py`) 
 4. offline augmentation of training data (`python dataset/offline_augment_images.py`)
