import logging
import sys
from typing import Optional, List

import efficientnet_pytorch
import torch
import torchvision
from torch import nn as nn
import pretrainedmodels

logger = logging.getLogger(__name__)


class AdaptiveConcatPool2d(nn.Module):
    """Layer that concats `AdaptiveAvgPool2d` and `AdaptiveMaxPool2d`."""

    def __init__(self, sz: Optional[int] = None):
        """Output will be 2*sz or 2 if sz is None"""
        super().__init__()
        self.output_size = sz or 1
        self.ap = nn.AdaptiveAvgPool2d(self.output_size)
        self.mp = nn.AdaptiveMaxPool2d(self.output_size)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return torch.cat([self.mp(x), self.ap(x)], 1)


class Flatten(nn.Module):
    """Flatten `x` to a single dimension, often used at the end of a model. `full` for rank-1 tensor"""

    def __init__(self, full: bool = False):
        super().__init__()
        self.full = full

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return x.view(-1) if self.full else x.view(x.size(0), -1)


class AlexNet(nn.Module):

    def __init__(self, **kwargs):
        super().__init__()

        pretrained = kwargs.get("pretrained", True)
        num_classes = kwargs.get("num_classes", 9)
        dropout = kwargs.get("dropout", 0.5)

        self.gradients = None

        self.model = torchvision.models.alexnet(pretrained=pretrained, progress=kwargs.get("progress", True))
        self.model.features = nn.Sequential(*self.model.features[:-1])  # remove last MaxPool2d layer
        self.model.max_pool = nn.MaxPool2d(kernel_size=3, stride=2)
        self.model.classifier = nn.Sequential(
            nn.Dropout(dropout),
            nn.Linear(256 * 6 * 6, 2048),
            nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(2048, 2048),
            nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(2048, 512),
            nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(512, num_classes)
        )

    def forward(self, x):
        x = self.features(x)
        if x.requires_grad:
            x.register_hook(self.save_gradients)
        x = self.model.max_pool(x)
        x = self.model.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.model.classifier(x)
        return x

    def save_gradients(self, gradients):
        self.gradients = gradients

    def features(self, x):
        return self.model.features(x)


class MobileNetV2(nn.Module):
    def __init__(self, dropouts=None, **kwargs):
        super().__init__()
        if dropouts is None:
            dropouts = [0.25, 0.5]
        pretrained = kwargs.get('pretrained', False)
        num_classes = kwargs.get('num_classes', 9)

        self.gradients = None

        self.model = torchvision.models.mobilenet_v2(pretrained)
        self.model.classifier = self.__classifier(dropouts, 2560, num_classes)

    def __classifier(self, dropouts: List[float], num_features: int, num_classes: int) -> nn.Sequential:
        return nn.Sequential(
            AdaptiveConcatPool2d(),
            Flatten(),
            nn.BatchNorm1d(num_features=num_features),
            nn.Dropout(p=dropouts[0]),
            nn.Linear(in_features=num_features, out_features=512),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(num_features=512),
            nn.Dropout(p=dropouts[1]),
            nn.Linear(in_features=512, out_features=num_classes)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.features(x)
        if x.requires_grad:
            x.register_hook(self.save_gradients)
        x = self.model.classifier(x)
        return x

    def save_gradients(self, gradients):
        self.gradients = gradients

    def features(self, x):
        return self.model.features(x)


class VGG(nn.Module):
    def __init__(self, dropouts: List[float] = None, **kwargs):
        super().__init__()
        if dropouts is None:
            dropouts = [0.25, 0.5, 0.5]
        pretrained = kwargs.get('pretrained', False)
        num_classes = kwargs.get('num_classes', 9)
        model_name = kwargs.get("model_name")

        self.gradients = None

        model_fct = getattr(torchvision.models, model_name)
        self.model: VGG = model_fct(pretrained)
        self.model.features = nn.Sequential(*self.model.features[:-1])  # remove last MaxPool2d layer
        self.model.max_pool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.model.avg_pool = nn.AdaptiveAvgPool2d(output_size=(7, 7))
        self.model.classifier = self.__classifier(dropouts, 1024, num_classes)

    def __classifier(self, dropouts: List[float], num_features: int, num_classes: int) -> nn.Sequential:
        return nn.Sequential(
            AdaptiveConcatPool2d(),
            Flatten(),
            nn.BatchNorm1d(num_features=num_features),
            nn.Dropout(p=dropouts[0]),
            nn.Linear(in_features=num_features, out_features=512),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(num_features=512),
            nn.Dropout(p=dropouts[1]),
            nn.Linear(in_features=512, out_features=256),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(num_features=256),
            nn.Dropout(p=dropouts[2]),
            nn.Linear(in_features=256, out_features=num_classes)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.features(x)
        if x.requires_grad:
            x.register_hook(self.save_gradients)
        x = self.model.max_pool(x)
        x = self.model.avg_pool(x)
        x = self.model.classifier(x)
        return x

    def save_gradients(self, gradients):
        self.gradients = gradients

    def features(self, x: torch.Tensor):
        return self.model.features(x)


class DenseNet(nn.Module):
    """
    pretrained model function names:
    densenet121
    densenet169
    densenet161
    densenet201
    """
    def __init__(self, **kwargs):
        super().__init__()
        pretrained = kwargs.get("pretrained")

        num_classes=kwargs.get("num_classes")
        if num_classes is None:
            logger.debug("Setting number of classes to default")
            num_classes = 9
        drop_rate=kwargs.get("drop_rate")
        if drop_rate is None:
            logger.debug("Setting dropout to default")
            drop_rate = 0
        if pretrained:
            model_name = kwargs.get("model_name")
            logger.debug("Loading pretrained model {}. Warning: All architecture settings will be ignored".format(model_name))
            #raise(NotImplementedError("Loading pretrained models with custom architecture is not supported yet."))
            progress = kwargs.get("progress")
            model_name = kwargs.get("model_name")
            if model_name is None:
                raise RuntimeError("Pretrained model with no model name selected.")
            model_fct = getattr(torchvision.models, model_name)
            self.model = model_fct(pretrained=True, progress=progress, drop_rate=drop_rate)
            if kwargs.get("fclayers") is None:
                self.model.classifier = FullyConnected(self.model.classifier.in_features, num_classes)
            else:
                self.model.classifier = FullyConnected(self.model.classifier.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))
        else:
            growth_rate=kwargs.get("growth_rate")
            block_config=kwargs.get("block_config")
            num_init_features=kwargs.get("num_init_features")
            bn_size=kwargs.get("bn_size")
            memory_efficient=kwargs.get("memory_efficient")
            if growth_rate is None:
                logger.debug("Setting growth rate to default")
                growth_rate = 32
            if block_config is None:
                logger.debug("Setting block config to default")
                block_config = (6, 12, 24, 16)
            if num_init_features is None:
                logger.debug("Setting num init features to default")
                num_init_features = 64
            if bn_size is None:
                logger.debug("Setting bottleneck size to default")
                bn_size = 4
            if memory_efficient is None:
                logger.debug("Setting memory efficiency to default")
                memory_efficient = False
            self.model = torchvision.models.DenseNet(growth_rate,
                                                     block_config,
                                                     num_init_features=num_init_features,
                                                     bn_size=bn_size,
                                                     drop_rate=drop_rate,
                                                     num_classes=num_classes)
    def freeze(self, freeze_layers):
        block_num = 0
        for name, module in self.named_modules():
            mod = str(module).split(" ")
            if "_DenseBlock" == mod[0][:-2] and block_num < freeze_layers:
                block_num += 1
                print("Freezing block {} with name {}".format(block_num, name))
                for layers in module.parameters():
                    layers.requires_grad = False
        if block_num != freeze_layers:
            logger.debug("Warning, wanted to freeze {} conv blocks, but only had {} blocks in the model".format(freeze_layers, block_num))


    def forward(self, x):
        return self.model.forward(x)

class ResNet(nn.Module):
    """
    pretrained model function names:
    resnet18
    resnet34
    resnet50
    resnet101
    resnet152
    """

    def __init__(self, **kwargs):
        super().__init__()
        pretrained = kwargs.get("pretrained")

        self.gradients = None

        num_classes = kwargs.get("num_classes")
        if num_classes is None:
            logger.debug("Setting number of classes to default")
            num_classes = 9

        if pretrained:
            model_name = kwargs.get("model_name")
            logger.debug("Loading pretrained model {}. Warning: All architecture settings will be ignored".format(model_name))
            #raise(NotImplementedError("Loading pretrained models with custom architecture is not supported yet."))
            progress = kwargs.get("progress")
            if model_name is None:
                raise RuntimeError("Pretrained model with no model name selected.")
            model_fct = getattr(torchvision.models, model_name)
            self.model = model_fct(pretrained=True, progress=progress)
            if kwargs.get("fclayers") is None:
                self.model.fc = FullyConnected(self.model.fc.in_features, num_classes)
            else:
                self.model.fc = FullyConnected(self.model.fc.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))
        else:
            layers=kwargs.get("layers")
            zero_init_residual=kwargs.get("zero_init_residual")
            groups=kwargs.get("groups")
            width_per_group=kwargs.get("width_per_group")
            replace_stride_with_dilation=kwargs.get("replace_stride_with_dilation")
            norm_layer=kwargs.get("norm_layer")
            if kwargs.get("block") is None:
                logger.debug("Setting block type to default: Bottleneck")
                block = torchvision.models.resnet.Bottleneck
            else:
                block = getattr(torchvision.models, kwargs.get("block"))
            if layers is None:
                logger.debug("Setting layer counts to default")
                layers = [3, 4, 23, 3]
            if zero_init_residual is None:
                logger.debug("Setting zero initialization of residual to default")
                zero_init_residual = False
            if groups is None:
                logger.debug("Setting groups parameter to default")
                groups = 1
            if width_per_group is None:
                logger.debug("Setting group width to default")
                width_per_group = 64
            self.model = torchvision.models.ResNet(block,
                                                   layers,
                                                   num_classes=num_classes,
                                                   zero_init_residual=zero_init_residual,
                                                   groups=groups,
                                                   width_per_group=width_per_group,
                                                   replace_stride_with_dilation=replace_stride_with_dilation,
                                                   norm_layer=norm_layer)
    def freeze(self, freeze_layers):
        block_num = 0
        for name, module in self.named_modules():
            mod = str(module).split(" ")
            if "Sequential" == mod[0][:-2] and block_num < freeze_layers:
                block_num += 1
                print("Freezing block {} with name {}".format(block_num, name))
                for layers in module.parameters():
                    layers.requires_grad = False
        if block_num != freeze_layers:
            logger.debug("Warning, wanted to freeze {} conv blocks, but only had {} blocks in the model".format(freeze_layers, block_num))

    def forward(self, x):
        x = self.features(x)

        if x.requires_grad:
            x.register_hook(self.save_gradients)

        x = self.model.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.model.fc(x)

        return x

    def save_gradients(self, gradients):
        self.gradients = gradients

    def features(self, x):
        x = self.model.conv1(x)
        x = self.model.bn1(x)
        x = self.model.relu(x)
        x = self.model.maxpool(x)

        x = self.model.layer1(x)
        x = self.model.layer2(x)
        x = self.model.layer3(x)
        x = self.model.layer4(x)
        return x

class SEResNet(nn.Module):
    """
    pretrained model function names:
    se_resnext50_32x4d
    se_resnext101_32x4d
    """

    def __init__(self, **kwargs):
        super().__init__()
        pretrained = kwargs.get("pretrained")

        self.gradients = None

        num_classes = kwargs.get("num_classes")
        if num_classes is None:
            logger.debug("Setting number of classes to default")
            num_classes = 9

        if pretrained:
            model_name = kwargs.get("model_name")
            logger.debug("Loading pretrained model {}. Warning: All architecture settings will be ignored".format(model_name))
            #raise(NotImplementedError("Loading pretrained models with custom architecture is not supported yet."))
            progress = kwargs.get("progress")
            if model_name is None:
                raise RuntimeError("Pretrained model with no model name selected.")
            model_fct = getattr(pretrainedmodels, model_name)
            self.model = model_fct()
            if kwargs.get("fclayers") is None:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes)
            else:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))
        else:
            model_fct = getattr(pretrainedmodels, model_name)
            self.model = model_fct(pretrained=False) 
            if kwargs.get("fclayers") is None:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes)
            else:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))


    def freeze(self, freeze_layers):
        block_num = 0
        for name, module in self.named_modules():
            mod = str(module).split(" ")
            if "Sequential" == mod[0][:-2] and block_num < freeze_layers:
                block_num += 1
                print("Freezing block {} with name {}".format(block_num, name))
                for layers in module.parameters():
                    layers.requires_grad = False
        if block_num != freeze_layers:
            logger.debug("Warning, wanted to freeze {} conv blocks, but only had {} blocks in the model".format(freeze_layers, block_num))

    def forward(self, x):
        x = self.model.features(x)
        if x.requires_grad:
            x.register_hook(self.save_gradients)
        x = self.model.logits(x)
        return x

    def save_gradients(self, gradients):
        self.gradients = gradients


class Inception(nn.Module):
    """
    pretrained model function names:
    inception_v3
    """

    def __init__(self, **kwargs):
        super().__init__()
        pretrained = kwargs.get("pretrained")
        init_weights = kwargs.get("init_weights")
        if init_weights is None:
            logger.debug("Setting init weights to default")
            init_weights = True

        num_classes=kwargs.get("num_classes")
        if num_classes is None:
            logger.debug("Setting number of classes to default")
            num_classes = 9
        if pretrained:
            model_name = kwargs.get("model_name")
            logger.debug("Loading pretrained model {}. Warning: All architecture settings will be ignored".format(model_name))
            progress = kwargs.get("progress")
            #raise(NotImplementedError("Loading pretrained models with custom architecture is not supported yet."))
            if model_name is None:
                raise RuntimeError("Pretrained model with no model name selected.")
            model_fct = getattr(torchvision.models, model_name)
            self.model = model_fct(pretrained=True, progress=progress, init_weights=init_weights)
            if kwargs.get("fclayers") is None:
                self.model.fc = FullyConnected(self.model.fc.in_features, num_classes)
            else:
                self.model.fc = FullyConnected(self.model.fc.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))
        else:
            aux_logits = kwargs.get("aux_logits")
            transform_input = kwargs.get("transform_input")
            inception_blocks = kwargs.get("inception_blocks")
            if aux_logits is None:
                logger.debug("Setting aux logits to default")
                aux_logits = True
            if transform_input is None:
                logger.debug("Setting transform input to default")
                transform_input = False

            self.model = torchvision.models.Inception3( num_classes=num_classes,
                                                        aux_logits=aux_logits,
                                                        transform_input=transform_input,
                                                        inception_blocks=inception_blocks,
                                                        init_weights=init_weights)

    def freeze(self, freeze_layers):
        block_num = 0
        for name, module in self.named_modules():
            mod = str(module).split(" ")
            if "Inception" == mod[0][:-3] and not mod[0][-3] == "3" and block_num < freeze_layers:
                block_num += 1
                print("Freezing block {} with name {}".format(block_num, name))
                for layers in module.parameters():
                    layers.requires_grad = False
        if block_num != freeze_layers:
            logger.debug("Warning, wanted to freeze {} conv blocks, but only had {} blocks in the model".format(freeze_layers, block_num))

    def forward(self, x):
        if self.model.training:
            return self.model.forward(x)[0]
        else:
            return self.model.forward(x)

class Xception(nn.Module):
    """
    pretrained model function names:
    xception
    """

    def __init__(self, **kwargs):
        super().__init__()
        pretrained = kwargs.get("pretrained")
        num_classes=kwargs.get("num_classes")
        if num_classes is None:
            logger.debug("Setting number of classes to default")
            num_classes = 9
        if pretrained:
            model_name = kwargs.get("model_name")
            logger.debug("Loading pretrained model {}. Warning: All architecture settings will be ignored".format(model_name))
            progress = kwargs.get("progress")
            if progress is False:
                print("Download progress bar of EfficientNet models can not be turned off")
            if model_name is None:
                raise RuntimeError("Pretrained model with no model name selected.")
            model_fct = getattr(pretrainedmodels, model_name)
            self.model = model_fct()
            if kwargs.get("fclayers") is None:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes)
            else:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))
        else:
            self.model = pretrainedmodels.xcpetion(pretrained=False)
            if kwargs.get("fclayers") is None:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes)
            else:
                self.model.last_linear = FullyConnected(self.model.last_linear.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))

    def freeze(self, freeze_layers):
        block_num = 0
        for name, module in self.model.named_modules():
            if ("block" == name[:-1] or "block" == name[:-2] or "conv" == name[:-1]) and block_num < freeze_layers:
                block_num += 1
                print("Freezing block {} with name {}".format(block_num, name))
                for layers in module.parameters():
                    layers.requires_grad = False
        if block_num != freeze_layers:
            logger.debug("Warning, wanted to freeze {} conv blocks, but only had {} blocks in the model".format(freeze_layers, block_num))

    def forward(self, x):
        """ Calls extract_features to extract features, applies final linear layer, and returns logits. """
        x = self.model.features(x)
        if x.requires_grad:
            x.register_hook(self.save_gradients)
        x = self.model.logits(x)
        return x

    def save_gradients(self, gradients):
        self.gradients = gradients

class EfficientNet(nn.Module):
    """
    pretrained model function names:
    efficientnet-b0
    efficientnet-b1
    efficientnet-b2
    efficientnet-b3
    efficientnet-b4
    efficientnet-b5
    efficientnet-b6
    efficientnet-b7
    efficientnet-b8 (advprop only)
    """
    def __init__(self, **kwargs):
        super().__init__()
        pretrained = kwargs.get("pretrained")

        self.gradients = None

        num_classes=kwargs.get("num_classes")
        if num_classes is None:
            logger.debug("Setting number of classes to default")
            num_classes = 9
        if pretrained:
            model_name = kwargs.get("model_name")
            logger.debug("Loading pretrained model {}. Warning: All architecture settings will be ignored".format(model_name))
            progress = kwargs.get("progress")
            if progress is False:
                print("Download progress bar of EfficientNet models can not be turned off")
            #raise(NotImplementedError("Loading pretrained models with custom architecture is not supported yet."))
            if model_name is None:
                raise RuntimeError("Pretrained model with no model name selected.")
            advprop = kwargs.get("advprop")
            if advprop is None:
                advprop = True
            self.model = efficientnet_pytorch.EfficientNet.from_pretrained(model_name, advprop=advprop)
            if kwargs.get("fclayers") is None:
                self.model._fc = FullyConnected(self.model._fc.in_features, num_classes)
            else:
                self.model._fc = FullyConnected(self.model._fc.in_features, num_classes, hidden_size=kwargs.get("fc_hidden_size"), num_layers=kwargs.get("fclayers"))
        else:
            model_name = kwargs.get("model_name")
            override_params = kwargs.get("override_params")
            if model_name is None:
                model_name = "efficientnet-b0"
            if override_params is None:
                override_params = {}
            override_params["num_classes"] = 9
            self.model = efficientnet_pytorch.EfficientNet.from_name(model_name, **override_params)

    def freeze(self, freeze_layers):
        block_num = 0
        for name, module in self.named_modules():
            mod = str(module).split(" ")
            if "MBConvBlock" == mod[0][:-2] and block_num < freeze_layers:
                block_num += 1
                print("Freezing block {} with name {}".format(block_num, name))
                for layers in module.parameters():
                    layers.requires_grad = False
        if block_num != freeze_layers:
            logger.debug("Warning, wanted to freeze {} conv blocks, but only had {} blocks in the model".format(freeze_layers, block_num))

    def forward(self, x):
        """ Calls extract_features to extract features, applies final linear layer, and returns logits. """
        bs = x.size(0)
        # Convolution layers
        x = self.features(x)

        if x.requires_grad:
            x.register_hook(self.save_gradients)

        # Pooling and final linear layer
        x = self.model._avg_pooling(x)
        x = x.view(bs, -1)
        x = self.model._dropout(x)
        x = self.model._fc(x)
        return x

    def save_gradients(self, gradients):
        self.gradients = gradients

    def features(self, x: torch.Tensor):
        return self.model.extract_features(x)


class FullyConnected(nn.Module):

    def __init__(self, num_inputs, num_outputs, hidden_size=None, num_layers=1):
        super().__init__()
        if hidden_size == None:
            hidden_size = max(num_inputs, 512)
        sizes = [num_inputs]
        sizes.extend([hidden_size] * (num_layers - 1))
        sizes.append(num_outputs)
        self.layers = nn.ModuleList([])
        for i in range(len(sizes) -1):
            self.layers.append(nn.Linear(sizes[i], sizes[i+1]))
            if i < len(sizes) - 2:
                self.layers.append(nn.ReLU()),
                self.layers.append(nn.BatchNorm1d(num_features=sizes[i+1])),
                self.layers.append(nn.Dropout(0.5))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

class Ensemble(nn.Module):

    def __init__(self, model_names, model_configs, gradient_boosted=False, weighted=False, learn_output=False):
        super().__init__()
        self.models = [ getattr(sys.modules[__name__], name)(**model_configs[i]) for i, name in enumerate(model_names)]
        self.gradient_boosted = gradient_boosted
        if learn_output:
            if weighted:
                logger.error("Weights will be ignored for ensemble output, as output is learned.")
            raise(NotImplementedError("Learning the final probability from the ensemble output is not yet supported"))
        self.learn_output = learn_output
        self.weighted = weighted

    def set_weights(self, weights):
        self.output_weights = weights
        self.output_weights.requires_grad = False

    def forward(self, x):
        #shape of input: (batch_size, num_channels, width, height)
        if self.gradient_boosted:
            raise(NotImplementedError("Gradient Boosting not supported yet"))
            #TODO find correct inputs for each of the models
            for model in self.models:
                out += model()
        else:
            predictions = torch.stack([model(x) for model in self.models], dim=0)
            if not self.weighted:
                mean_prediciton = torch.mean(predictions, dim=0)
            else:
                mean_prediction = torch.mean(predictions*self.output_weights, dim=0)*(torch.sum(self.output_weights) ** -1)
            return mean_prediction, predictions
