import glob
from pathlib import Path

from PIL import Image
from joblib import Parallel, delayed
from tqdm.auto import tqdm

WRITE_DATA = False


def scale_image(image: Image, new_size: int) -> Image:
    """
    scale the shortest side of `image` to `new_size`

    :param image: image to scale
    :param new_size: new length of the shortest size
    :return: scaled image
    """
    width, height = image.size
    if width > height:
        new_height = new_size
        new_width = round(new_height * width / height)
    else:
        new_width = new_size
        new_height = round(new_width * height / width)
    return image.resize((new_width, new_height), Image.BICUBIC)


def center_crop_image(image: Image, new_size: int) -> Image:
    """
    center crop image to size `new_size`x`new_size`, image is scaled automatically

    :param image: image to crop
    :param new_size:
    :return:
    """
    scaled_image = scale_image(image, new_size)
    width, height = scaled_image.size

    left = (width - new_size) / 2
    top = (height - new_size) / 2
    right = (width + new_size) / 2
    bottom = (height + new_size) / 2

    return scaled_image.crop((left, top, right, bottom))


def modify_image(file_name: str) -> None:
    """
    scale and copy image

    :param file_name: file name of the image
    :return: None
    """
    image = center_crop_image(Image.open(file_name), 512)
    new_file_name = file_name.replace('data/', 'data_cropped/')
    if WRITE_DATA:
        Path('/'.join(new_file_name.split('/')[:-1])).mkdir(parents=True, exist_ok=True)  # create missing folder
        image.save(f'{".".join(new_file_name.split(".")[:-1])}.jpg')


if __name__ == '__main__':
    Parallel(n_jobs=-1)(delayed(modify_image)(file_name) for file_name in tqdm(glob.glob('../data/**/*.*', recursive=True)))  # parallel execution of modify_image
