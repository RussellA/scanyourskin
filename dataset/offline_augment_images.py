import glob
from pathlib import Path

import numpy as np
from PIL import Image
from imgaug import augmenters as iaa
from joblib import Parallel, delayed
from tqdm.auto import tqdm

WRITE_DATA = False

DATA_PATH = 'data_split/train'


def work(file_name: str, aug_factor: int) -> None:
    """
    generate `aug_factor` images of `file_name` one original and `aug_factor` - 1 augmented versions

    :param file_name: path to file to augment
    :param aug_factor: number of copies of the original file
    :return: None
    """
    seq_offline = iaa.Sequential([
        iaa.Affine(rotate=(-180, 180), scale=(0.8, 1.0), mode=["reflect", "symmetric"]),
        iaa.PerspectiveTransform(scale=(0.05, 0.2)),
        iaa.Sometimes(0.5, iaa.AdditiveGaussianNoise(scale=(1, 15))),
        iaa.Sometimes(0.5, iaa.JpegCompression(compression=(80, 90))),
        iaa.Sometimes(0.5, iaa.MultiplySaturation((0.8, 1.2))),
        iaa.Sometimes(0.5, iaa.AveragePooling(kernel_size=(2, 4))),
        iaa.Sometimes(0.5, iaa.pillike.EnhanceSharpness(factor=(0.0, 5.0))),
        iaa.BlendAlphaSimplexNoise(iaa.OneOf([
            iaa.EdgeDetect(alpha=(0.0, 0.2)),
            iaa.DirectedEdgeDetect(alpha=(0.0, 0.2), direction=(0.0, 1.0)),
        ])),
    ], random_order=True)

    file_name_new = file_name.replace(f'{DATA_PATH}', 'data_augmented/train')
    image = Image.open(file_name)
    for idx in range(aug_factor):
        if WRITE_DATA:
            if idx == 0:
                Path('/'.join(file_name_new.split('/')[:-1])).mkdir(parents=True, exist_ok=True)  # create missing folder
                image.save(file_name_new)
            else:
                temp_file_name = file_name_new.replace('.jpg', f'_aug_{idx - 1:02d}.jpg')
                Image.fromarray(seq_offline(image=np.asarray(image))).save(temp_file_name)


def main():
    aug_factors = {'VASC': 45, 'SCC': 22, 'NV': 1, 'BKL': 4, 'BCC': 3, 'DF': 36, 'AK': 16, 'UNK': 1, 'MEL': 3}  # values calculated by hand
    for class_name, aug_factor in aug_factors.items():
        file_names = glob.glob(f'../{DATA_PATH}/{class_name}/*')
        Parallel(n_jobs=-1)(delayed(work)(file_name, min(aug_factor, 21)) for file_name in tqdm(file_names))  # max 21 copies of an image


if __name__ == '__main__':
    main()
