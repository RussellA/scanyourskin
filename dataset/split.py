import glob
from collections import Counter
from pathlib import Path
from shutil import copy2
from typing import List, Tuple

from sklearn.model_selection import StratifiedShuffleSplit

WRITE_DATA = False


def get_file_names_and_classes(path: str = '../data_cropped/**/*.*') -> Tuple[list, list]:
    """
    get a tuple ot all filenames and their class

    :param path: path of dataset
    :return: tuple of filenames and classes
    """
    file_names = []
    classes = []
    for file_name in glob.glob(path, recursive=True):
        file_names.append(file_name)
        classes.append(file_name.split('/')[2])

    return file_names, classes


def num_of_elements_per_class(y: List[str], relative_value: bool = False) -> List[Tuple[str, float]]:
    """
    count how many entry of an element are in `y`

    :param y: list of strings
    :param relative_value: if True calculate relative value
    :return: list of entry names and how often it appeared in the list
    """
    counter = Counter(y)
    if relative_value:
        return sorted([(key, round(value / sum(counter.values()), 5)) for key, value in counter.items()], key=lambda x: x[0])
    else:
        return sorted([(key, value) for key, value in counter.items()], key=lambda x: x[0])


if __name__ == '__main__':
    sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, train_size=0.6, random_state=42)  # split data "preserving the percentage of samples for each class"

    file_names, _ = get_file_names_and_classes()
    X, y = get_file_names_and_classes()

    train_index, test_index = next(sss.split(X, y))
    print(f"Length Test: {len(train_index)}")
    print(f"Length Train: {len(test_index)}")

    train_y = []
    train_file_names = []
    for idx in train_index:
        file_names.remove(X[idx])
        train_file_names.append(X[idx])
        train_y.append(y[idx])

    test_y = []
    test_file_names = []
    for idx in test_index:
        file_names.remove(X[idx])
        test_file_names.append(X[idx])
        test_y.append(y[idx])

    classes_distribution_all = Counter(y)
    classes_distribution_train = Counter(train_y)
    classes_distribution_test = Counter(test_y)
    valid_file_names = file_names
    classes_distribution_valid = Counter([file_name.split('/')[2] for file_name in file_names])

    print(f"Length Validation: {len(file_names)}")

    print('all data  ', num_of_elements_per_class(y, True))
    print('train data', num_of_elements_per_class(train_y, True))
    print('test data ', num_of_elements_per_class(test_y, True))
    print('valid data', num_of_elements_per_class([file_name.split('/')[2] for file_name in file_names], True))
    print('valid data', num_of_elements_per_class([file_name.split('/')[2] for file_name in file_names]))

    for file_name in train_file_names:
        new_file_name = file_name.replace('data_cropped', 'data_split/train')
        if WRITE_DATA:
            Path('/'.join(new_file_name.split('/')[:-1])).mkdir(parents=True, exist_ok=True)  # create missing folder
            copy2(file_name, new_file_name)

    for file_name in test_file_names:
        new_file_name = file_name.replace('data_cropped', 'data_split/test')
        if WRITE_DATA:
            Path('/'.join(new_file_name.split('/')[:-1])).mkdir(parents=True, exist_ok=True)  # create missing folder
            copy2(file_name, new_file_name)

    for file_name in valid_file_names:
        new_file_name = file_name.replace('data_cropped', 'data_split/valid')
        if WRITE_DATA:
            Path('/'.join(new_file_name.split('/')[:-1])).mkdir(parents=True, exist_ok=True)  # create missing folder
            copy2(file_name, new_file_name)
