# for Reproducibility
import numpy as np
import pandas as pd
import torch
from fastai.vision import models, cnn_learner, DatasetType, Learner, Dataset
from torch.utils.data import SequentialSampler

from v2_fastai.fast_ai import setup_data

torch.manual_seed(42)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(42)

CLASSES = ['AK', 'BCC', 'BKL', 'DF', 'MEL', 'NV', 'SCC', 'VASC', 'UNK']


def get_preds(ds_type: DatasetType, dataset: Dataset, file_name_csv: str, num_runs: int = 1) -> None:
    all_preds = []
    all_classes = []
    all_file_names = []

    for idx in range(num_runs):
        preds, y, losses = learn.get_preds(ds_type=ds_type, with_loss=True)
        all_preds.extend(list(preds.numpy()))
        all_classes.extend(list(y.numpy()))
        all_file_names.extend([f'{path.stem}-{idx}{path.suffix}' for path in dataset.x._relative_item_paths()])

    data_csv = []
    for pred, y, file_name in zip(all_preds, all_classes, all_file_names):
        data_csv.append([file_name, *pred, y])

    data_df = pd.DataFrame(data_csv, columns=['file_name', *CLASSES, 'y'])
    data_df = data_df.set_index('file_name')
    data_df.to_csv(file_name_csv)


if __name__ == '__main__':
    data = setup_data('dataset_sd_224_v3', bs=16)

    data.train_dl.dl.batch_sampler.sampler = SequentialSampler(data.train_dl.dl.dataset)  # override RandomSampler
    # data.train_ds.tfms = []  # disable all augmentations
    # data.valid_ds.tfms = []  # disable all augmentations

    learn: Learner = cnn_learner(data, models.mobilenet_v2)
    learn.load('bestmodel-0.754461944103241')

    get_preds(DatasetType.Train, data.train_ds, '../training/train_df.csv', num_runs=10)
    get_preds(DatasetType.Valid, data.valid_ds, '../training/valid_df.csv')
