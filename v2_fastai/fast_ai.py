import time
import warnings
from functools import partial
from typing import Tuple, Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
from fastai.callback import Callback, add_metrics
from fastai.callbacks import CSVLogger, SaveModelCallback
from fastai.vision import ImageDataBunch, imagenet_stats, cnn_learner, models, accuracy, ClassificationInterpretation, Learner, Callable, DatasetType, get_transforms, cutout, pil2tensor, TfmPixel
from imgaug import augmenters as iaa
from sklearn.metrics import balanced_accuracy_score

CLASSES = ['AK', 'BCC', 'BKL', 'DF', 'MEL', 'NV', 'SCC', 'VASC', 'UNK']


class balanced_accuracy(Callback):
    def __init__(self, threshold=0.):
        self.threshold = threshold
        if threshold > 0.:
            self.name = f'balanced_acc_>=_{threshold:1.2f}'
        else:
            self.name = 'balanced_acc'

    def on_epoch_begin(self, **kwargs):
        self.targs, self.preds = torch.Tensor([]), torch.Tensor([])

    def on_batch_end(self, last_output, last_target, **kwargs):
        y_pred_conf, y_pred = torch.max(last_output, 1)
        y_pred_confident = [prediction if np.max(pred_conf) >= self.threshold else -1 for pred_conf, prediction in zip(list(y_pred_conf.cpu().numpy()), list(y_pred.cpu().numpy()))]
        y_pred_confident = torch.Tensor(y_pred_confident)
        assert y_pred_confident.numel() == last_target.numel(), "Expected same numbers of elements in pred & targ"
        self.preds = torch.cat((self.preds, y_pred_confident.cpu().float()))
        self.targs = torch.cat((self.targs, last_target.cpu().float()))

    def on_epoch_end(self, last_metrics, **kwargs):
        with warnings.catch_warnings():  # ignore warnings from scikit-learn
            warnings.simplefilter("ignore")
            bma = balanced_accuracy_score(self.targs.cpu().numpy(), self.preds.cpu().numpy())
        result = torch.Tensor([bma])
        return add_metrics(last_metrics, result)


def setup_data(path: str, bs: int = 16, tfms=None) -> ImageDataBunch:
    def tensor2np(x):
        np_image = x.cpu().permute(1, 2, 0).numpy()
        np_image = (np_image * 255).astype(np.uint8)
        return np_image

    def alb_tfm2fastai(alb_tfm):
        def _alb_transformer(x):
            np_image = tensor2np(x)
            transformed = alb_tfm(image=np_image)
            tensor_image = pil2tensor(transformed, np.float32)
            tensor_image.div_(255)
            return tensor_image

        transformer = TfmPixel(_alb_transformer)

        return transformer()

    augmentation = iaa.Sequential([
        iaa.Sometimes(0.5, iaa.CropAndPad(percent=(-0.2, 0.2), pad_mode=["reflect", "symmetric"])),
        iaa.Sometimes(0.5, iaa.Affine(rotate=(-180, 180), translate_percent=(-0.2, 0.2), scale=(0.8, 1.2), mode=["reflect", "symmetric"])),

        iaa.SomeOf((2, 5), [
            iaa.OneOf([iaa.ShearX((-20, 20), mode=["reflect", "symmetric"]), iaa.ShearY((-20, 20), mode=["reflect", "symmetric"])]),
            iaa.OneOf([iaa.GaussianBlur(sigma=(0.0, 3.0)), iaa.AverageBlur(k=(1, 9))]),
            iaa.ChangeColorTemperature((5000, 9000)),
            iaa.GammaContrast((0.7, 1.2)),
            iaa.MultiplyAndAddToBrightness(mul=(0.9, 1.1), add=(-10, 10)),
            iaa.MotionBlur(k=(5, 15), angle=(-45, 45)),
            iaa.pillike.EnhanceColor((0.8, 1.3)),
        ])
    ], random_order=True)

    if tfms is None:
        tfms = get_transforms(do_flip=True, flip_vert=True, max_rotate=360,
                              xtra_tfms=[
                                  alb_tfm2fastai(augmentation),
                                  cutout(n_holes=(0, 4), length=(16, 16), p=.5)
                              ])

    return ImageDataBunch.from_folder(
        path,
        classes=CLASSES,
        test='test',
        ds_tfms=tfms,
        bs=bs
    ).normalize(imagenet_stats)


def plot_data_dist(data: ImageDataBunch):
    vc = pd.concat([
        pd.value_counts(data.train_ds.y.items, sort=True),
        pd.value_counts(data.valid_ds.y.items, sort=True)
    ], axis=1)
    vc.columns = ['train', 'valid']
    vc.index = ['NV', 'UNK', 'MEL', 'BCC', 'BKL', 'AK', 'SCC', 'DF', 'VASC']  # TODO find a better way than hardcoding the order
    vc.plot(kind='bar', title='data distribution')
    plt.show()


def setup_learner(base_arch: Callable, data: ImageDataBunch, name: str) -> Learner:
    return cnn_learner(data, base_arch, pretrained=True,
                       metrics=[accuracy, balanced_accuracy(), balanced_accuracy(0.5)],
                       callback_fns=[
                           partial(CSVLogger, filename=f'history-{name}', append=True),
                           partial(SaveModelCallback, monitor='balanced_acc', name=f'bestmodel-{name}'),
                           # partial(LearnerTensorboardWriter, base_dir=Path(f'tensorboard/{NAME}'), name='run1'),
                           # MixedPrecision,  # Use fp16 to take advantage of tensor cores on recent NVIDIA GPUs (Nvidia Volta + GeForce 20 series) for a 200% or more speedup.
                       ])


def find_lr(learner: Learner, name: str, save_fig: bool = False, return_fig: bool = False) -> Optional[plt.Figure]:
    learner.lr_find(num_it=1000, end_lr=1)
    fig = learner.recorder.plot(suggestion=True, return_fig=True)
    if save_fig:
        fig.savefig(f'{name}-LRFinder-{time.time()}.png')
    if return_fig:
        return fig


def classification_interp(learner: Learner, name: str, save_fig: bool = False, return_fig: bool = False) -> Optional[Tuple[plt.Figure, plt.Figure]]:
    interp = ClassificationInterpretation.from_learner(learner)

    print(interp.most_confused(min_val=20))
    fig_top_losses = interp.plot_top_losses(16, figsize=(11, 11), return_fig=True)
    fig_confusion_matrix = interp.plot_confusion_matrix(normalize=True, return_fig=True)
    if save_fig:
        fig_top_losses.savefig(f'{name}-top_losses-{time.time()}.png')
        fig_confusion_matrix.savefig(f'{name}-confusion_matrix-{time.time()}.png')

    if return_fig:
        return fig_top_losses, fig_confusion_matrix


def predict_test_data(learner: Learner) -> pd.DataFrame:
    preds, y = learner.get_preds(ds_type=DatasetType.Test)

    csv_data = []
    for file_name, pred in zip(learner.data.test_ds.items, preds.numpy()):
        data = [str(file_name).split('/')[-1]]
        data.extend(pred)
        csv_data.append(data)

    result_df = pd.DataFrame(columns=['image'] + CLASSES, data=csv_data)
    # TODO scale results?
    return result_df


if __name__ == '__main__':
    NAME = 'mobilenet_v2'

    data = setup_data('dataset_sd_224')
    learner = setup_learner(models.mobilenet_v2, data, NAME)

    learner.freeze()

    find_lr(learner, NAME, save_fig=True)
    learner.fit_one_cycle(1, max_lr=1.13E-03)  # TODO update max_lr

    classification_interp(learner, NAME, save_fig=True)
    # TODO maybe save current model before continue training

    learner.unfreeze()
    find_lr(learner, NAME, save_fig=True)
    learner.fit_one_cycle(1, max_lr=1.0E-05)  # TODO update max_lr
    # TODO maybe save current model before continue training

    test_df = predict_test_data(learner)
    test_df.to_csv(f'{NAME}-results.csv', index=False)
