import os
import sys
from datetime import date
from typing import Dict

import yaml

from training.train import run_training

module_path = os.path.abspath(os.path.join('.'))
if module_path not in sys.path:
    sys.path.append(module_path)


def parse_yaml(file_path: str, create_folder: bool = True) -> Dict:
    print("Reading paramfile {}".format(file_path))

    with open(file_path) as f:
        param = yaml.load(f, Loader=yaml.FullLoader)

    if param.get("load_model", False):
        param["save_dir"] = param["load_model"]
    elif create_folder:
        # Find save directory which doesn't exist yet
        save_dir = str(param["model_type"]) + "_" + "".join(str(date.today()).split("-")[1:])
        save_dir_mod = save_dir + "_0"
        i = 1
        while os.path.exists(save_dir_mod):
            save_dir_mod = save_dir + "_" + str(i)
            i += 1

        os.mkdir(save_dir_mod)
        param["save_dir"] = save_dir_mod
    else:
        raise RuntimeError("No folders can be created in read-only mode.")

    return param


def run(file_path: str, callback=run_training):
    params = parse_yaml(file_path)
    callback(**params)


if __name__ == "__main__":
    run(sys.argv[1])
