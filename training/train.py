import os
import time
import warnings
from functools import partial
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import torch
from imgaug import augmenters as iaa
from sklearn.metrics import f1_score, confusion_matrix, ConfusionMatrixDisplay, balanced_accuracy_score
from torch.utils.data import DataLoader, WeightedRandomSampler
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms
from tqdm.auto import tqdm, trange

import model_collection.model_zoo
from training.losses import get_loss
from utils.dataloader import ImageDataSet

device = "cpu"
if torch.cuda.is_available():
    device = "cuda"
print("Using device {}".format(device))

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])


def get_augmentations(image_size):
    return transforms.Compose([
        transforms.Resize((image_size[0], image_size[1])),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        np.asarray,
        iaa.Sequential([
            iaa.Sometimes(0.5, iaa.CropAndPad(percent=(-0.2, 0.2), pad_mode=["reflect", "symmetric"])),
            iaa.Sometimes(0.5, iaa.Affine(rotate=(-180, 180), translate_percent=(-0.2, 0.2), scale=(0.8, 1.2), mode=["reflect", "symmetric"])),
            iaa.Dropout(p=(0, 0.5)),

            iaa.SomeOf((2, 5), [
                iaa.OneOf([iaa.ShearX((-20, 20), mode=["reflect", "symmetric"]), iaa.ShearY((-20, 20), mode=["reflect", "symmetric"])]),
                iaa.OneOf([iaa.GaussianBlur(sigma=(0.0, 3.0)), iaa.AverageBlur(k=(1, 9))]),
                iaa.ChangeColorTemperature((5000, 9000)),
                iaa.GammaContrast((0.7, 1.2)),
                iaa.MultiplyAndAddToBrightness(mul=(0.9, 1.1), add=(-10, 10)),
                iaa.MotionBlur(k=(5, 15), angle=(-45, 45)),
                iaa.pillike.EnhanceColor((0.8, 1.3)),
            ])
        ], random_order=True).augment_image,
        transforms.ToTensor(),
        normalize
    ])


def create_dataloaders(data_path, batch_size, image_size, weighted_sampling, return_path=False, unk_limit=None):
    train_set = ImageDataSet(root_dir=os.path.join(data_path, "train"), transform=get_augmentations(image_size), include_unk=True, return_path=return_path, unk_limit=unk_limit)
    val_set = ImageDataSet(root_dir=os.path.join(data_path, "valid"), transform=transforms.Compose([transforms.Resize((image_size[0], image_size[1])), transforms.ToTensor(), normalize]), include_unk=True, return_path=return_path)
    weights, weight_per_class = train_set.get_balanced_weights()

    if weighted_sampling:
        sampler = WeightedRandomSampler(torch.DoubleTensor(weights), len(weights))
        dataloader_train = DataLoader(train_set, batch_size=batch_size, sampler=sampler, num_workers=os.cpu_count(), drop_last=True)
    else:
        dataloader_train = DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=os.cpu_count(), drop_last=True)
    dataloader_val = DataLoader(val_set, batch_size=batch_size, shuffle=False, num_workers=os.cpu_count())

    return {"train": dataloader_train, "val": dataloader_val}, weight_per_class


def calc_accuracy_per_class(accuracy_per_class: List[dict], predicted: torch.Tensor, labels: torch.Tensor) -> List[dict]:
    for label, predicted_label in zip(labels, predicted):
        if label == predicted_label:
            accuracy_per_class[label]['right'] += 1
        else:
            accuracy_per_class[label]['wrong'] += 1

    return accuracy_per_class


def train_model(model, dataloaders, criterion, optimizer, scheduler, tb, num_epochs=1, start_epoch=0, save_dir="Model", overwrite_chkpt=True):
    tqdm.write('| epoche | train loss | val loss | accuracy | avg. acc |   F1   |   BMA  | train time |')
    tqdm.write('|--------|------------|----------|----------|----------|--------|--------|------------|')
    best_val = 100
    for epoch in trange(start_epoch, start_epoch + num_epochs, desc='Epoch', leave=True, position=0):
        train_loss = 0.0
        correct = 0
        total = 0

        loss_log = {"train": 0, "val": 0}
        training_time = 0
        time_start = 0
        y_true = []
        y_pred = []
        y_pred_confident = []

        accuracy_per_class = [{'right': 0, 'wrong': 0} for _ in list(dataloaders.values())[0].dataset.class_numbers]

        for phase in ["train", "val"]:
            if phase == "train":
                time_start = time.time()
                model.train()
            else:
                model.eval()
            running_loss = 0.0

            for inputs, labels in tqdm(dataloaders[phase], desc=f'Batch {phase}', leave=False, position=1):
                inputs = inputs.to(device)
                labels = labels.long().to(device)
                optimizer.zero_grad()
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    loss = criterion(outputs, labels)
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                        scheduler.step()
                    else:
                        pred_conf, predicted = torch.max(outputs.data, 1)
                        total += labels.size(0)
                        y_true.extend(list(labels.cpu().numpy()))
                        y_pred.extend(list(predicted.cpu().numpy()))
                        y_pred_confident.extend([prediction if np.max(pred_conf) >= 0.5 else -1 for pred_conf, prediction in zip(list(pred_conf.cpu().numpy()), list(predicted.cpu().numpy()))])
                        correct += (predicted == labels).sum().item()
                        accuracy_per_class = calc_accuracy_per_class(accuracy_per_class, predicted, labels)
                    running_loss += loss.item() * inputs.size(0)
            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            loss_log[phase] = epoch_loss

            if phase == "train":
                training_time = time.time() - time_start
                train_loss = epoch_loss
                with open(os.path.join(save_dir, "tr_loss.txt"), "a+") as loss_file:
                    loss_file.write(f"{epoch}, {epoch_loss}\n")

        add_confusion_matrix_to_tb(epoch, tb, y_pred, y_true)
        accuracy = 100. * correct / total
        average_acc = balanced_accuracy_score_no_warings(y_true, y_pred) * 100.
        f1_macro = f1_score(y_true, y_pred, average='macro')
        bma = balanced_accuracy_score_no_warings(y_true, y_pred_confident)

        with open(os.path.join(save_dir, "loss.txt"), "a+") as loss_file:
            loss_file.write(f"{epoch}, {epoch_loss}\n")

        minute, sec = divmod(training_time, 60)
        tqdm.write('|     {:02d} |     {:1.4f} |   {:1.4f} |  {:2.4f} |  {:2.4f} | {:1.4f} | {:1.4f} |      {:02d}:{:02d} |'
                   .format(epoch, loss_log["train"], loss_log["val"], accuracy, average_acc, f1_macro, bma, round(minute), round(sec)))
        tb.add_scalars("Train_Val Loss", {"train_loss": train_loss, "val_loss": epoch_loss}, epoch)
        tb.add_scalars("Accuracy", {"accuracy": accuracy, "avg. accuracy per class": average_acc}, epoch)

        accuracy_per_class_tensorboard = {}
        for key, value in list(dataloaders.values())[0].dataset.class_numbers.items():
            elem = accuracy_per_class[value]
            accuracy_per_class_tensorboard[key] = elem['right'] / (elem['right'] + elem['wrong']) * 100.
        tb.add_scalars('accuracy_per_class', accuracy_per_class_tensorboard, epoch)
        tb.add_scalars('F-1', {"micro": f1_score(y_true, y_pred, average="micro"), "macro": f1_macro,
                               "weighted": f1_score(y_true, y_pred, average='weighted')}, epoch)

        # Save Model
        if not overwrite_chkpt:
            path = os.path.join(save_dir, str(model).split("(")[0] + "_" + str(epoch))
        else:
            path = os.path.join(save_dir, str(model).split("(")[0])

        # Save and overwrite Model with best validation loss
        if epoch_loss < best_val:
            path = path + "_best"
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': running_loss,
            }, f"{path}.tar")
            best_val = epoch_loss

        torch.save({
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': running_loss,
        }, f"{path}.tar")


def add_confusion_matrix_to_tb(epoch: int, tb: SummaryWriter, y_pred: List[int], y_true: List[int]) -> None:
    """
    add confusion matrix plot to TensorBoard

    @param epoch: step value to record
    @param tb: SummaryWriter of TensorBoard
    @param y_pred: Ground truth (correct) target values.
    @param y_true: Estimated targets as returned by a classifier.
    @return:
    """
    cm = confusion_matrix(y_true, y_pred) * 100.
    _, ax = plt.subplots()
    ax.imshow = partial(ax.imshow, vmin=0, vmax=100)
    cmd = ConfusionMatrixDisplay(cm, display_labels=["AK", "BCC", "BKL", "DF", "MEL", "NV", "SCC", "VASC", "UNK"])
    tb.add_figure("ConfusionMatrix", cmd.plot(cmap=plt.cm.Blues, ax=ax).figure_, global_step=epoch)


def balanced_accuracy_score_no_warings(y_true: List[int], y_pred_confident: List[int]) -> float:
    """
    simply wraps balanced_accuracy_score from scikit-learn

    @param y_true: Ground truth (correct) target values.
    @param y_pred_confident: Estimated targets as returned by a classifier.
    @return: balanced accuracy
    """
    with warnings.catch_warnings():  # ignore warnings from scikit-learn
        warnings.simplefilter("ignore")
        bma = balanced_accuracy_score(y_true, y_pred_confident)
    return bma


def run_training(**param):
    model = getattr(model_collection.model_zoo, param.get("model_type"))(**param.get("model_params")).to(device)

    freeze_layers = param.get("freeze_layers")
    if not freeze_layers is None and not freeze_layers == 0:
        model.freeze(freeze_layers)


    optimizer = getattr(torch.optim, param.get("optimizer"))(model.parameters(), **param.get("optimizer_params"))
    image_size = param.get("image_size")
    if image_size is None:
        image_size = (224, 224)
    dataloaders, weights = create_dataloaders(param.get("data_path"), param.get("batch_size"), image_size, param.get("weighted_sampling"), unk_limit=param.get("unk_limit"))

    class dummy_scheduler():
        def __init__(self):
            pass

        def step(self):
            pass

    lr_scheduler = dummy_scheduler()
    if param.get("one_cycle"):
        lr_scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, param.get("optimizer_params")["lr"], epochs = param.get("epochs"), steps_per_epoch=len(dataloaders["train"]))

    criterion = get_loss(param, weights, device)


    epoch = 0
    if param.get("load_model"):
        state_dicts = torch.load(param.get("load_model")+f"/{param.get('model_type')}.tar", map_location=device)
        model.load_state_dict(state_dicts["model_state_dict"])
        optimizer.load_state_dict(state_dicts["optimizer_state_dict"])
        epoch = state_dicts["epoch"]+1
        opt_param = param.get("optimizer_params")
        if not opt_param.get("weight_decay") is None and not opt_param.get("weight_decay") == optimizer.param_groups[0]["weight_decay"]:
            optimizer.param_groups[0]["weight_decay"] = opt_param["weight_decay"]
        if not opt_param.get("lr") is None and not opt_param.get("lr") == optimizer.param_groups[0]["lr"]:
            optimizer.param_groups[0]["lr"] = opt_param["lr"]
    # initialize tensorboard
    tb = SummaryWriter(f"training/runs/{param['save_dir']}")
    #tb.add_hparams(param.get("optimizer_params"),{})

    train_model(model=model,
                dataloaders=dataloaders,
                criterion=criterion,
                optimizer=optimizer,
                scheduler=lr_scheduler,
                tb=tb,
                num_epochs=param.get("epochs"),
                start_epoch=epoch,
                save_dir=param["save_dir"])

    tb.close()
