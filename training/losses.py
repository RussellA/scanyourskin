import torch


def get_loss(param, weights, device, **kwargs):
    loss_name = param.get("loss_function")
    if loss_name is None:
        return torch.nn.CrossEntropyLoss(**kwargs)
    elif loss_name == "focal_loss":
        return FocalLoss(weights, device, **kwargs)
    elif loss_name == "inverse_class_prob":
        inv_prob = torch.FloatTensor(weights).to(device)
        inv_prob = inv_prob/torch.sum(inv_prob)
        inv_prob.requires_grad = False
        return torch.nn.CrossEntropyLoss(inv_prob, **kwargs)
    elif loss_name == "linear_class_prob":
        class_prob = torch.FloatTensor(weights).to(device) ** (-1)
        class_prob = class_prob/torch.sum(class_prob)
        class_prob.requires_grad = False
        return torch.nn.CrossEntropyLoss(-class_prob + 1, **kwargs)


class FocalLoss():
    def __init__(self, weights, device, alpha=1, gamma=1):
        self.weights = torch.FloatTensor(weights).to(device)
        self.weights.requires_grad = False
        self.num_classes = len(weights)
        self.alpha = alpha
        self.gamma = gamma

    def __call__(self, output, labels, reduction='mean'):
        softmax = torch.nn.functional.softmax(output, dim=1)
        #print(labels.repeat(self.num_classes, 1).T.shape)
        #print(torch.arange(self.num_classes).repeat(len(labels), 1).shape)
        pt = torch.where(labels.repeat(self.num_classes, 1).T == torch.arange(self.num_classes).repeat(len(labels), 1).to(labels.device),
                        softmax,
                        1-softmax)
        loss_per_item = - torch.sum( self.weights.unsqueeze(0) * self.alpha * ((1 - pt) ** self.gamma) * torch.log(pt) , dim = 1) /torch.mean(self.weights)
        if reduction == 'mean':
            return torch.mean(loss_per_item)
        elif reduction == 'none':
            return loss_per_item
        else:
            raise NotImplementedError
