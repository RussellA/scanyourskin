import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '..'))  # workaround to import (main.py) from parent dir

import argparse
import math
from collections import OrderedDict
from typing import Tuple

import matplotlib.patheffects as pe
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import transforms
from tqdm.auto import tqdm

from main import parse_yaml
from model_collection import model_zoo
from training.losses import get_loss
from training.train import create_dataloaders


class NormalizeInverse(transforms.Normalize):
    '''
    Undoes the normalization and returns the reconstructed images in the input domain.
    copied from https://discuss.pytorch.org/t/simple-way-to-inverse-transform-normalization/4821/8
    '''

    def __init__(self, mean, std):
        mean = torch.as_tensor(mean)
        std = torch.as_tensor(std)
        std_inv = 1 / (std + 1e-7)
        mean_inv = -mean * std_inv
        super().__init__(mean=mean_inv, std=std_inv)

    def __call__(self, tensor):
        return super().__call__(tensor.clone())


def evaluate_model(model: nn.Module, dataloader: DataLoader, criterion, device: str) -> Tuple[list, list, list, list]:
    y_true = []
    y_pred = []
    losses = []
    pred_acc = []

    model.eval()
    for inputs, labels in tqdm(dataloader):
        inputs = inputs.to(device)
        labels = labels.long().to(device)

        with torch.set_grad_enabled(False):
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            losses.extend(list(loss.cpu().numpy()))

            predicted_acc, predicted_class = torch.max(torch.softmax(outputs.data, 1), 1)
            pred_acc.extend(list(predicted_acc.cpu().numpy()))
            y_true.extend(list(labels.cpu().numpy()))
            y_pred.extend(list(predicted_class.cpu().numpy()))

    return y_true, y_pred, losses, pred_acc


def load_pretrained_model(file_path: str, model, device: str) -> None:
    state_dicts = torch.load(file_path, map_location=device)
    model.load_state_dict(state_dicts['model_state_dict'])


def calc_heat_map(model: nn.Module, image_normalized: torch.Tensor, y: int) -> np.asarray:
    """
    calculate Grad-CAM as described https://arxiv.org/abs/1610.02391

    @param model: model for predictions
    @param image_normalized: single normalized input image
    @param y: label
    @return: heat map of the `image_normalized` according to Grad-CAM
    """
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    image_normalized = image_normalized.unsqueeze(dim=0).to(device)

    model.eval()
    pred = model(image_normalized)
    pred[:, y].backward()  # get the gradient of the output with respect to the parameters of the model

    gradients = model.gradients
    pooled_gradients = torch.mean(gradients, dim=[0, 2, 3])  # pool gradients across the channels
    activations = model.features(image_normalized).detach()  # get the activations of the last feature layer

    # weight the channels by corresponding gradients
    for i in range(activations.shape[1]):
        activations[:, i, :, :] *= pooled_gradients[i]

    heat_map = torch.mean(activations, dim=1).squeeze().cpu()
    heat_map = np.maximum(heat_map, 0)  # ReLU for better visualization
    heat_map /= torch.max(heat_map)  # normalize heatmap

    return heat_map.squeeze().cpu().numpy()


def plot_top_losses(k: int, dataloader: DataLoader, losses_dict: dict, model: nn.Module = None, figsize: Tuple[int, int] = (12, 12)):
    """
    plot images `k` images sorted by loss

    @param k: number of images to plot
    @param dataloader: PyTorch DataLoader
    @param losses_dict: dict of losses, has to be sorted
    @param model: pytorch model, needed for GradCam
    @param figsize: width, height in inches of figure
    @return: Figure of images
    """
    normalize_inverse = NormalizeInverse(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])  # hardcoded values :(

    classes = dict((v, k) for k, v in dataloader.sampler.data_source.class_numbers.items())
    cols = math.ceil(math.sqrt(k))
    rows = math.ceil(k / cols)

    fig, axes = plt.subplots(rows, cols, figsize=figsize)
    fig.suptitle('Prediction / Actual / Loss / Probability', weight='bold', size=30, path_effects=[pe.withStroke(linewidth=5, foreground='w')])

    #for i, (loss, (idx, y_true, y_pred)) in enumerate(list(losses_dict.items())[:k]):
    for i, (loss, (idx, y_true, y_pred, pred_acc)) in enumerate(list(losses_dict.items())[:k]):
        image_normalized, y_true_dataloader_val = dataloader.dataset[idx]
        assert y_true == y_true_dataloader_val, '`y_true` does not match'

        image = normalize_inverse(image_normalized)
        image_pil = transforms.ToPILImage()(image)
        axes.flat[i].imshow(image_pil)
        axes.flat[i].set_title(f'{classes[y_pred]} / {classes[y_true]} / {loss:.2f} / {pred_acc:.2f}')
        axes.flat[i].axis('off')

        if model:
            heat_map = calc_heat_map(model, image_normalized, y_true)
            heat_map[np.isnan(heat_map)] = 0
            axes.flat[i].imshow(heat_map, alpha=0.4, extent=(0, *image.shape[-2:], 0), interpolation='bilinear', cmap='magma')

    plt.tight_layout()
    return fig


def main(file_path: str, display_heat_map: bool = False, largest: bool = True) -> None:
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    params = parse_yaml(file_path, create_folder=False)
    model = getattr(model_zoo, params.get('model_type'))(**params.get('model_params')).to(device)
    image_size = params.get('image_size', (224, 224))
    dataloaders, weights = create_dataloaders(params.get('data_path'), params.get('batch_size'), image_size, params.get('weighted_sampling'))
    criterion = get_loss(params, weights, device, reduction='none')

    load_pretrained_model(params.get('load_model') + f'/{params.get("model_type")}.tar', model, device)

    dataloader_val = dataloaders['val']
    y_trues, y_preds, losses, pred_accs = evaluate_model(model, dataloader_val, criterion, device)
    losses_dict = OrderedDict(sorted({loss: (idx, y_trues[idx], y_preds[idx], pred_accs[idx]) for idx, loss in enumerate(losses)}.items(), reverse=largest))

    if not display_heat_map:
        model = None
    fig = plot_top_losses(16, dataloader_val, losses_dict, model)
    fig.savefig(f'top_losses-{params.get("load_model")}.png')
    plt.show()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, required=True)
    parser.add_argument('--heat_map', required=False, dest='heat_map', action='store_true')
    parser.add_argument('--lowest_loss', required=False, dest='biggest_loss', action='store_false')
    parser.set_defaults(heat_map=False, biggest_loss=True)
    args = parser.parse_args()

    main(args.config, args.heat_map, args.biggest_loss)
